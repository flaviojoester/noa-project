#!/bin/bash

set -e

printf "\n"
printf '==================================================================================\n'
printf '========== SCRIPT DE DEPLOY EM PRODUCAO ============\n'
printf '==================================================================================\n'
printf "\n"

printf "============== Limpando todas as imagens locais do Docker ==============\n"
printf "\n"
docker system prune -f --all
printf "\n"
printf "============== Docker Local Limpo ==============\n"
printf "\n"
printf "============== Construido o Projeto com Maven Local ou Runner ==============\n"
printf "\n"
mvn clean install package -Dmaven.test.skip=true
printf "\n"
printf "============ Projeto Construido com Sucesso pelo Maven ===========\n"
printf "\n"

printf "== Executando o Docker Compose Com Build para subir a aplicação ==\n"
printf "\n"

docker-compose -f $PWD/docker-compose.yml up -d --build