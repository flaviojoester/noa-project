package br.com.noa.models;

import java.io.Serializable;
import java.util.List;

public class Channel implements Serializable {

	private static final long serialVersionUID = 1L;

	private String title;
	private String link;
	private String description;
	private String lastBuildDate;
	private List<Item> item;

	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public String getDescription() {
		return description;
	}

	public String getLastBuildDate() {
		return lastBuildDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Item> getItem() {
		return item;
	}

	@Override
	public String toString() {
		return "News [title=" + title + ", link=" + link + ", description=" + description + ", lastBuildDate="
				+ lastBuildDate + ", itens=" + item + "]";
	}

}