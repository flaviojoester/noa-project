package br.com.noa.models;

import java.io.Serializable;

public class Item implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private String link;
	private String pubDate;
	private String description;

	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "Item [title=" + title + ", link=" + link + ", pubDate=" + pubDate + ", description=" + description
				+ "]";
	}

}