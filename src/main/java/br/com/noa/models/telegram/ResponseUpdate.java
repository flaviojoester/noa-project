package br.com.noa.models.telegram;

import java.io.Serializable;
import java.util.List;

public class ResponseUpdate implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean ok;
	public List<Result> result;

	public Boolean getOk() {
		return ok;
	}

	public void setOk(Boolean ok) {
		this.ok = ok;
	}

	public List<Result> getResult() {
		return result;
	}

	public void setResult(List<Result> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "Hook [ok=" + ok + ", result=" + result + "]";
	}

}