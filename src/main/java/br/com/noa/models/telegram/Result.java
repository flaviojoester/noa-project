package br.com.noa.models.telegram;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

public class Result implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonbProperty(value = "update_id")
	private Long id;

	private Message message;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Result [id=" + id + ", message=" + message + "]";
	}

}