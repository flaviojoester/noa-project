package br.com.noa.models.telegram;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tb_chat")
public class Chat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String type;

	private String username;

	@JsonbProperty(value = "first_name")
	private String firstName;

	@JsonbProperty(value = "last_name")
	private String lastName;

	@JsonbTransient
	private Timestamp timestamp;

	@JsonbTransient
	@Version
	private Integer version;

	@JsonIgnore
	@JsonbTransient
	@OneToMany(mappedBy = "chat", cascade = CascadeType.ALL)
	private List<Message> messages;

	public Chat() {
		this.timestamp = Timestamp.from(Instant.now());
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public List<Message> getMessages() {
		return messages;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chat other = (Chat) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Chat [id=" + id + ", lastName=" + lastName + ", type=" + type + ", firstName=" + firstName
				+ ", username=" + username + "]";
	}

}