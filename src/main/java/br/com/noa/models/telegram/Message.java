package br.com.noa.models.telegram;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "tb_message")
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@JsonbProperty(value = "message_id")
	private Long id;

	@Column(length = 5000)
	private String text;

	private Long date;

	@ManyToOne
	@JoinColumn(name = "from_id")
	private From from;

	@ManyToOne
	@JoinColumn(name = "chat_id")
	private Chat chat;

	@JsonbTransient
	private Boolean isAnswer;

	@JsonbTransient
	private Timestamp timestamp;

	@JsonbTransient
	@Version
	private Integer version;

	public Message() {
		this.isAnswer = false;
		this.timestamp = Timestamp.from(Instant.now());
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public From getFrom() {
		return from;
	}

	public void setFrom(From from) {
		this.from = from;
	}

	public Chat getChat() {
		return chat;
	}

	public void setChat(Chat chat) {
		this.chat = chat;
	}

	public Boolean getIsAnswer() {
		return isAnswer;
	}

	public void setIsAnswer(Boolean isAnswer) {
		this.isAnswer = isAnswer;
	}

	public Instant getTimestamp() {
		return timestamp.toInstant();
	}

	@Override
	public int hashCode() {
		return Objects.hash(chat, from, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		return Objects.equals(chat, other.chat) && Objects.equals(from, other.from) && Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", text=" + text + ", date=" + date + ", from=" + from + ", chat=" + chat
				+ ", isAnswer=" + isAnswer + ", timestamp=" + timestamp + "]";
	}

}