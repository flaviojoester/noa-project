package br.com.noa.models.telegram;

import java.io.Serializable;

public class ResponseSend implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean ok;
	public Message result;

	public Boolean getOk() {
		return ok;
	}

	public void setOk(Boolean ok) {
		this.ok = ok;
	}

	public Message getResult() {
		return result;
	}

	public void setResult(Message result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "ReturnSend [ok=" + ok + ", result=" + result + "]";
	}

}