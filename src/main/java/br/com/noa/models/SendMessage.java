package br.com.noa.models;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

public class SendMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonbProperty(value = "chat_id")
	private Integer id;
	private String text;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}