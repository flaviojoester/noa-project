package br.com.noa.models;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class BookAddress {

	@Inject
	@ConfigProperty(name = "telegram.token", defaultValue = "")
	String token;

	public String getToken() {
		return token;
	}
}