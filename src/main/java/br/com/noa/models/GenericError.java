package br.com.noa.models;

import java.io.Serializable;
import java.time.Instant;

import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.annotation.JsonFormat;

public class GenericError implements Serializable {

	private static final long serialVersionUID = 1L;

//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Instant timestamp;
	private Status status;
	private String error;
	private String message;
	private String path;

	public GenericError() {
		this.timestamp = Instant.now();
	}

	public GenericError(Status status, String error, String message, String path) {
		super();
		this.status = status;
		this.error = error;
		this.message = message;
		this.path = path;
		this.timestamp = Instant.now();
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "GenericError [timestamp=" + timestamp + ", status=" + status + ", error=" + error + ", message="
				+ message + ", path=" + path + "]";
	}

}
