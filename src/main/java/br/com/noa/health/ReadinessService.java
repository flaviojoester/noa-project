package br.com.noa.health;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Readiness;

import br.com.noa.exceptions.SuperPersistenceException;
import br.com.noa.services.MessageService;

@Readiness
@ApplicationScoped
public class ReadinessService implements HealthCheck {

	private static final HealthCheckResponseBuilder BUILDER = HealthCheckResponse.named("App Readiness check");

	@Inject
	MessageService messageService;

	@Inject
	public ReadinessService() {
		BUILDER.withData("message", "Mel na chupeta!");
		BUILDER.up();
	}

	@Override
	public HealthCheckResponse call() {

		checkDatabase();

		return BUILDER.build();
	}

	private void checkDatabase() {
		try {
			messageService.findAll();
		} catch (SuperPersistenceException e) {
			BUILDER.withData("message", e.getMessage());
			BUILDER.down();
		}
	}
}