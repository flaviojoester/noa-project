package br.com.noa.health;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;

@Liveness
@ApplicationScoped
public class LivenessService implements HealthCheck {

	private static final HealthCheckResponseBuilder BUILDER = HealthCheckResponse.named("App health check");

	@Inject
	public LivenessService() {
		BUILDER.withData("message", "Xuxu beleza!");
		BUILDER.up();
	}

	@Override
	public HealthCheckResponse call() {
		return BUILDER.build();
	}
}