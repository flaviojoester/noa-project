package br.com.noa.enums;

public enum BotReactions {
	HI("saldação", "precisa de ajuda?", "Quer Ajuda?"),
	OK("confirmacao", "Belezinha :)", "Show :)"),
	NO("negação", "claro que não", "necas de pitiribiribas"),
	DOUBT("duvida", "como é que é!?", "oxe!"),
	WAIT("espera", "Aguenta aê", "pera, dois palitos!");

	String title;
	String message;
	String alternative;

	private BotReactions(String title, String message, String alternative) {
		this.title = title;
		this.message = message;
		this.alternative = alternative;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAlternative() {
		return alternative;
	}

	public void setAlternative(String alternative) {
		this.alternative = alternative;
	}

}