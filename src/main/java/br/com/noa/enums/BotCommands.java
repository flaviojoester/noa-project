package br.com.noa.enums;

import java.util.Arrays;
import java.util.Optional;

import javax.enterprise.inject.Instance;

public enum BotCommands {
	AJUDA("Ajuda", "Precisa de ajuda?", "/help", null),

	COMANDO_INVALIDO("Comandos", "Vou listar os comandos validos pra você.", "/commands", null),

	COMANDO("Comando", "Qual comando você quer tentar?.", "/command", null),

	NEWS("Noticias", "Quero ver as noticias.", "/news", null),

	JESUS("Versiculo Biblico", "Quero meditar na palavra do Senhor.", "/JESUS", null),

	MONEY_TODAY("Dolar Hoje", " Segue o valor do dolar hoje!", "/money", null);

	String title;
	String message;
	String command;
	Instance<?> instance;

	private BotCommands(String title, String message, String command, Instance<?> instance) {
		this.title = title;
		this.message = message;
		this.command = command;
		this.instance = instance;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Instance<?> getInstance() {
		return instance;
	}

	public void setInstance(Instance<?> instance) {
		this.instance = instance;
	}

	public static Optional<BotCommands> filter(String text) {
		return Arrays.stream(values()).filter(e -> e.getCommand().contentEquals(text)
				|| e.getTitle().contentEquals(text) || e.getTitle().contains(text)).findFirst();
	}
}