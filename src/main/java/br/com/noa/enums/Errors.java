package br.com.noa.enums;

public enum Errors {
	ERRO_APLICACAO("000", "Erro ao executar", "%s", "%s", "%s"),
	TEXTO_ENTRADA_NAO_INFORMADO("004", "Texto de entrada nao foi informado", "", "", ""),
	ERRO_DISPARO_TEAMS("005", "Não foi possivel enviar no teams", "", "", "");

	String mensagem;
	String codigo;

	String userHelp;
	String developerMessage;
	String moreInfo;

	private Errors(String mensagem, String codigo, String userHelp, String developerMessage, String moreInfo) {
		this.mensagem = mensagem;
		this.codigo = codigo;
		this.userHelp = userHelp;
		this.developerMessage = developerMessage;
		this.moreInfo = moreInfo;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public String getUserHelp() {
		return userHelp;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public String getMoreInfo() {
		return moreInfo;
	}

}
