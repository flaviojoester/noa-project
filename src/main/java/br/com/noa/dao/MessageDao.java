package br.com.noa.dao;

import javax.enterprise.context.RequestScoped;
import javax.persistence.PersistenceException;

import org.eclipse.microprofile.opentracing.Traced;

import br.com.noa.exceptions.SuperPersistenceException;
import br.com.noa.models.telegram.Message;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@Traced
@RequestScoped
public class MessageDao implements PanacheRepository<Message> {

	public Message insertOrUpdate(Message entity) throws SuperPersistenceException {
		try {
			persistAndFlush(entity);
			return entity;
		} catch (PersistenceException e) {
			throw new SuperPersistenceException(String.format("Erro ao gravar '%s' no banco de dados. Erro: %s",
					entity.getClass(), e.getMessage()));
		}
	}
}