package br.com.noa.rest;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.noa.exceptions.SuperException;
import br.com.noa.models.telegram.ResponseUpdate;
import br.com.noa.services.TelegramService;

@Path("/telegram")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TelegramResources {

	@Inject
	TelegramService telegram;

	private final static Logger logger = LoggerFactory.getLogger(TelegramResources.class);

	@PostConstruct
	public void init() throws SuperException {
		logger.info("consumindo api");
	}

	@GET
	@Path("/getUpdates")
	@Transactional(rollbackOn = SuperException.class)
	@Operation(summary = "Retorna uma lista de mensagens")
	@APIResponse(responseCode = "200", //
			content = @Content(//
					mediaType = MediaType.APPLICATION_JSON, //
					schema = @Schema(//
							implementation = ResponseUpdate.class, //
							type = SchemaType.ARRAY)))
	public ResponseUpdate getUpdates() throws SuperException {
		return telegram.getUpdates();
	}
}