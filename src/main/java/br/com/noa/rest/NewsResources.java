package br.com.noa.rest;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.noa.InterfaceNews;
import br.com.noa.exceptions.SuperException;

@Path("/news")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NewsResources {

	@Inject
	Instance<InterfaceNews> news;

	private final static Logger logger = LoggerFactory.getLogger(NewsResources.class);

	@PostConstruct
	public void init() throws SuperException {
		logger.info("consumindo api de noticias");
	}

	@GET
	@Path("/news")
	@Operation(summary = "Receber Noticias")
	@APIResponse(responseCode = "200", //
			content = @Content(//
					mediaType = MediaType.APPLICATION_JSON, //
					schema = @Schema(//
							implementation = Object.class, //
							type = SchemaType.ARRAY)))
	public Object getMews() throws SuperException {
		return news.get().push("opiniao");
	}
}