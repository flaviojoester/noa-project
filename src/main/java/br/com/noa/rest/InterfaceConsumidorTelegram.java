package br.com.noa.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * To use it via injection.
 *
 * {@code @Inject
 * 
 * @RestClient MyRemoteService myRemoteService;
 *
 * public void doSomething() { Set<MyRemoteService.Extension>
 * restClientExtensions =
 * myRemoteService.getExtensionsById("io.quarkus:quarkus-rest-client"); } }
 */
@RegisterRestClient(configKey = "telegram.api")
@RequestScoped
public interface InterfaceConsumidorTelegram extends AutoCloseable {

	@Timeout(value = 5000)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/bot{token}/getUpdates")
	Response getUpdates(@PathParam("token") String token, @QueryParam(value = "offset") Integer offset);

	@Timeout(value = 5000)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/bot{token}/sendMessage")
	Response sendMessage(@PathParam("token") String token, @QueryParam("chat_id") Long id,
			@QueryParam("text") String text);

	void close();
}