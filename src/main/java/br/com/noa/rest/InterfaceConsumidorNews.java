package br.com.noa.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * To use it via injection.
 *
 * {@code @Inject
 * 
 * @RestClient MyRemoteService myRemoteService;
 *
 * public void doSomething() { Set<MyRemoteService.Extension>
 * restClientExtensions =
 * myRemoteService.getExtensionsById("io.quarkus:quarkus-rest-client"); } }
 */
@RegisterRestClient(configKey = "news.api")
@RequestScoped
public interface InterfaceConsumidorNews extends AutoCloseable {

	@Timeout(value = 5000)
	@GET
	@Produces(MediaType.TEXT_XML)
	@Path("/rss/{offset}")
	Response get(@PathParam(value = "offset") String offset);
}