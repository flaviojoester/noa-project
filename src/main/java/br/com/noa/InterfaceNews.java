package br.com.noa;

import br.com.noa.exceptions.SuperException;
import br.com.noa.models.Channel;

public interface InterfaceNews {

	Channel push(String offset) throws SuperException;

}