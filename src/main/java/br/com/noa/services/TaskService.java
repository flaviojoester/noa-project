package br.com.noa.services;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.noa.InterfaceBotRoutine;
import br.com.noa.exceptions.SuperException;
import br.com.noa.exceptions.SuperPersistenceException;
import br.com.noa.models.telegram.Chat;
import br.com.noa.models.telegram.From;
import br.com.noa.models.telegram.Message;
import br.com.noa.models.telegram.ResponseSend;
import br.com.noa.models.telegram.ResponseUpdate;
import br.com.noa.models.telegram.Result;
import io.quarkus.scheduler.Scheduled;
import io.quarkus.scheduler.Scheduled.ConcurrentExecution;

@ApplicationScoped
public class TaskService {

	@Inject
	TelegramService telegramService;

	@Inject
	Instance<InterfaceBotRoutine> botRoutine;

	@Inject
	MessageService messageService;

	@Inject
	ChatService chatService;

	@Inject
	FromService fromService;

	private static final Logger logger = LoggerFactory.getLogger(TaskService.class);

	@Transactional(rollbackOn = SuperPersistenceException.class)
	@Scheduled(every = "5s", delay = 0, delayUnit = TimeUnit.MINUTES, concurrentExecution = ConcurrentExecution.SKIP, identity = "TELEGRAM-INTERACTIVE-JOB")
	public void cronJob() throws SuperException, SuperPersistenceException {

		ResponseUpdate response = telegramService.getUpdates();

		for (Result result : response.getResult()) {

			Message message = result.getMessage();

			if (message == null) {
				continue;
			}

			Chat chat = message.getChat();
			From from = message.getFrom();

			try {
				chat = chatService.findById(chat.getId());
			} catch (SuperPersistenceException e) {
				logger.error(e.getMessage());
				chat = chatService.insertOrUpdate(chat);
			} finally {
				message.setChat(chat);
			}

			try {
				from = fromService.findById(from.getId());
			} catch (SuperPersistenceException e) {
				logger.error(e.getMessage());
				from = fromService.insertOrUpdate(from);
			} finally {
				message.setFrom(from);
			}

			try {
				message = messageService.findById(message.getId());
			} catch (SuperPersistenceException e) {
				logger.info(String.format("Nova interação: %s", e.getMessage()));
				message = messageService.insertOrUpdate(message);
			}
			logger.info(String.format("Message: %s -> %s", message.getText(), message.getTimestamp()));
		}
	}

	@Transactional(rollbackOn = SuperPersistenceException.class)
	@Scheduled(every = "5s", delay = 0, delayUnit = TimeUnit.MINUTES, concurrentExecution = ConcurrentExecution.SKIP, identity = "TELEGRAM-UPDATE-JOB")
	public void cronJobing() throws SuperException, SuperPersistenceException {
		List<Message> messages = messageService.findByAllStatus();

		logger.info("quantidade: " + messages.size());

		for (Message message : messages) {

			if (!message.getIsAnswer()) {

				String text = botRoutine.get().route(message);

				ResponseSend responseSend = telegramService.sendMessage(message, text);
				message.setIsAnswer(responseSend.getOk());
				messageService.insertOrUpdate(message);
			}

		}
	}
}