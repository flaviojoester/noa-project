package br.com.noa.services;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.faulttolerance.exceptions.TimeoutException;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.noa.exceptions.SuperException;
import br.com.noa.models.BookAddress;
import br.com.noa.models.telegram.Message;
import br.com.noa.models.telegram.ResponseSend;
import br.com.noa.models.telegram.ResponseUpdate;
import br.com.noa.rest.InterfaceConsumidorTelegram;

@Dependent
public class TelegramService {

	@Inject
	@RestClient
	InterfaceConsumidorTelegram consumidor;

	@Inject
	BookAddress book;

	private static final Logger logger = LoggerFactory.getLogger(TelegramService.class);

	@Inject
	TelegramService() throws SuperException {
		logger.info("-----------># Lendo o topico do Telegram #<---------------");
	}

	public ResponseSend sendMessage(Message message, String text) throws SuperException {
		Response response = null;
		try {
			response = consumidor.sendMessage(book.getToken(), message.getChat().getId(), text);

			if (response == null) {
				throw new SuperException("Comunicação indisponivel.");
			} else if (response.getStatus() == 429) {
				throw new SuperException("Hook Bloqueado por excesso de mensagem.");
			} else if (response.getStatus() != 200) {
				throw new SuperException("Comunicação indisponivel.");
			}

			return response.readEntity(ResponseSend.class);
		} catch (WebApplicationException | TimeoutException | IllegalStateException e) {
			throw new SuperException(e);
		} finally {
			int status = 0;
			if (response != null) {
				status = response.getStatus();
				response.close();
			}

			logger.info(String.format("Telegram SendMessage Status: %s, Message: %s", status, text));
		}
	}

	public ResponseUpdate getUpdates() throws SuperException {
		Response response = null;
		try {
			response = consumidor.getUpdates(book.getToken(), -1);

			if (response == null) {
				throw new SuperException("Comunicação indisponivel.");
			} else if (response.getStatus() == 429) {
				throw new SuperException("Hook Bloqueado por excesso de envio de mensagens.");
			} else if (response.getStatus() != 200) {
				throw new SuperException("Comunicação indisponivel com o telegram.");
			}

			return response.readEntity(ResponseUpdate.class);
		} catch (WebApplicationException | TimeoutException | IllegalStateException e) {
			throw new SuperException(e);
		} finally {
			int status = 0;
			if (response != null) {
				status = response.getStatus();
				response.close();
			}
			logger.info(String.format("Telegram getUpdates Status: %s", status));
		}
	}
}