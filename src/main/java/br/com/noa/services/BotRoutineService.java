package br.com.noa.services;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.io.output.StringBuilderWriter;

import br.com.noa.InterfaceBotRoutine;
import br.com.noa.InterfaceNews;
import br.com.noa.enums.BotCommands;
import br.com.noa.exceptions.SuperException;
import br.com.noa.models.Channel;
import br.com.noa.models.Item;
import br.com.noa.models.telegram.Message;

@RequestScoped
public class BotRoutineService implements InterfaceBotRoutine {

	@Inject
	Instance<InterfaceNews> news;

	private BotCommands botCommands = BotCommands.AJUDA;
	private StringBuilderWriter builder = new StringBuilderWriter();

	@Override
	public String route(Message message) throws SuperException {

		botCommands = BotCommands.filter(message.getText()).orElse(BotCommands.COMANDO_INVALIDO);

		switch (botCommands.getCommand()) {
			case "/news":
				getNews();
				break;
			case "/help":
				getHelp();
				break;
			case "/command":
				getCommand();
				break;
			case "/commands":
				getCommands();
				break;
			case "/JESUS":
				getCutWordOfLordJesus();
				break;
			default:
				getDefault();
				break;
		}
		return builder.toString();
	}

	private void getCutWordOfLordJesus() throws SuperException {
		builder.append("Que as bençãos do Senhor se derrame sobre vida hoje!");
	}

	private void getCommand() throws SuperException {
		builder.append("Deus tem um plano para sua vida!");
	}

	private void getNews() throws SuperException {
		Channel channel = news.get().push("opiniao");

		builder.append(String.format("[ %s ] \n", channel.getTitle().toUpperCase()));

		builder.append(String.format("%s\n", channel.getDescription()));
		builder.append(String.format("%s\n", channel.getLastBuildDate()));

		builder.append("\n");
//		builder.append(String.format("Abrir ->[%s]\n", channel.getLink()));
//
		for (Item item : channel.getItem()) {
			builder.append(String.format("**%s** \n", item.getTitle()));
//			builder.append(String.format("__%s__\n", item.getDescription()));
			builder.append(String.format("%s\n", item.getPubDate()));
//			builder.append(String.format("Abrir -> [ %s ] \n", item.getLink()));
			builder.append("\n");
		}

	}

	private void getHelp() throws SuperException {
		builder.append(botCommands.getMessage());
		builder.append("\nBlza. Digita então o que você precisa!");
	}

	private void getCommands() throws SuperException {
		builder.append(botCommands.getMessage());

		BotCommands[] botMessages = BotCommands.values();
		for (BotCommands botMsg : botMessages) {
			builder.append(String.format("\n %s -> %s", botMsg.getTitle(), botMsg.getCommand()));
		}
	}

	private void getDefault() throws SuperException {
		builder.append("Não entendi o que você quer. ");
		builder.append("Tenta um desses comandos:\n");
		getCommands();
	}
}