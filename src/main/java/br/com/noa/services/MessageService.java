package br.com.noa.services;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.LockModeType;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.transaction.Transactional;

import br.com.noa.dao.MessageDao;
import br.com.noa.exceptions.SuperPersistenceException;
import br.com.noa.models.telegram.Message;

@Dependent
public class MessageService {

	@Inject
	MessageDao dao;

	@Transactional(rollbackOn = SuperPersistenceException.class)
	public Message insertOrUpdate(Message entity) throws SuperPersistenceException {
		return dao.insertOrUpdate(entity);
	}

	public List<Message> findAll() throws SuperPersistenceException {
		try {
			return dao.listAll();
		} catch (PersistenceException e) {
			throw new SuperPersistenceException(
					String.format("Erro ao listar '%s' no banco de dados. Erro: %s", dao.getClass(), e.getMessage()));
		}
	}

	public Message findById(Long id) throws SuperPersistenceException {
		try {
			return dao.findByIdOptional(id, LockModeType.OPTIMISTIC_FORCE_INCREMENT)
					.orElseThrow(() -> new SuperPersistenceException(
							String.format("Entidade: %s ID: %s não encontrado!", dao.getClass(), id)));
		} catch (OptimisticLockException e) {
			throw new SuperPersistenceException(
					String.format("Entidade '%s' bloqueada. Erro: %s", dao.getClass(), e.getMessage()));
		}
	}

	public List<Message> findByAllStatus() throws SuperPersistenceException {
		try {
			return dao.find("isAnswer", false).withLock(LockModeType.PESSIMISTIC_FORCE_INCREMENT).list();
		} catch (PessimisticLockException e) {
			throw new SuperPersistenceException(
					String.format("Entidade '%s' bloqueada. Erro: %s", dao.getClass(), e.getMessage()));
		} catch (PersistenceException e) {
			throw new SuperPersistenceException(
					String.format("Erro ao filtrar '%s' no banco de dados. Erro: %s", dao.getClass(), e.getMessage()));
		}
	}
}