package br.com.noa.services;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.eclipse.microprofile.faulttolerance.exceptions.TimeoutException;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.noa.InterfaceNews;
import br.com.noa.exceptions.SuperException;
import br.com.noa.models.Channel;
import br.com.noa.rest.InterfaceConsumidorNews;

@Dependent
public class NewsService implements InterfaceNews {

	@Inject
	@RestClient
	InterfaceConsumidorNews consumidor;

	private final static Logger logger = LoggerFactory.getLogger(NewsService.class);

	@Override
	public Channel push(String offset) throws SuperException {
		Response response = null;
		try {
			response = consumidor.get(String.format("%s.xml", offset));

			if (response == null) {
				throw new SuperException("Comunicação indisponivel.");
			} else if (response.getStatus() != 200) {
				throw new SuperException("Comunicação indisponivel com o rss.");
			}

			String msg = response.readEntity(String.class);

			JSONObject jObject = XML.toJSONObject(msg);

			jObject = jObject.getJSONObject("rss");
			String parse = jObject.getJSONObject("channel").toString();

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			return mapper.readValue(parse, Channel.class);
		} catch (WebApplicationException | TimeoutException | IllegalStateException | JsonProcessingException e) {
			throw new SuperException(e);
		} finally {
			int status = 0;
			if (response != null) {
				status = response.getStatus();
				response.close();
			}
			logger.info(String.format("News RSS Status: %s", status));
		}
	}
}