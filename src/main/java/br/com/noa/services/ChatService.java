package br.com.noa.services;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.transaction.Transactional;

import br.com.noa.dao.ChatDao;
import br.com.noa.exceptions.SuperPersistenceException;
import br.com.noa.models.telegram.Chat;

@Dependent
public class ChatService {

	@Inject
	ChatDao dao;

	@Transactional(rollbackOn = SuperPersistenceException.class)
	public Chat insertOrUpdate(Chat entity) throws SuperPersistenceException {
		return dao.insertOrUpdate(entity);
	}

	public List<Chat> findAll() throws SuperPersistenceException {
		try {
			return dao.listAll();
		} catch (PersistenceException e) {
			throw new SuperPersistenceException(
					String.format("Erro ao listar '%s' no banco de dados. Erro: %s", dao.getClass(), e.getMessage()));
		}
	}

	public Chat findById(Long id) throws SuperPersistenceException {
		try {
			return dao.findByIdOptional(id, LockModeType.PESSIMISTIC_FORCE_INCREMENT)
					.orElseThrow(() -> new SuperPersistenceException(
							String.format("Entidade: %s ID: %s não encontrado!", dao.getClass(), id)));
		} catch (PessimisticLockException e) {
			throw new SuperPersistenceException(
					String.format("Entidade '%s' bloqueada. Erro: %s", dao.getClass(), e.getMessage()));
		}
	}
}