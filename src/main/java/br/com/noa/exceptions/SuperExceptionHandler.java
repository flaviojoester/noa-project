package br.com.noa.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.noa.models.GenericError;

@Provider
public class SuperExceptionHandler implements ExceptionMapper<SuperException> {

	@Override
	public Response toResponse(SuperException exception) {

		GenericError error = new GenericError(Status.BAD_REQUEST, exception.getLocalizedMessage(),
				exception.getMessage(), exception.getClass().getName());

		return Response.status(Status.BAD_REQUEST).entity(error).build();
	}

}