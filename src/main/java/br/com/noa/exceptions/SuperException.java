package br.com.noa.exceptions;

public class SuperException extends Exception {

	private static final long serialVersionUID = 1L;

	public SuperException(Throwable e) {
		super(e);
	}

	public SuperException(String message) {
		super(message);
	}
}