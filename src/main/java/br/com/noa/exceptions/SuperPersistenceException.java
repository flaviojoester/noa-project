package br.com.noa.exceptions;

public class SuperPersistenceException extends Exception {

	private static final long serialVersionUID = 1L;

	public SuperPersistenceException(Throwable e) {
		super(e);
	}

	public SuperPersistenceException(String message) {
		super(message);
	}
}