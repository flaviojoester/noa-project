package br.com.noa.exceptions;

public class SubException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SubException(Throwable e) {
		super(e);
	}

	public SubException(String message) {
		super(message);
	}
}