package br.com.noa;

import br.com.noa.exceptions.SuperException;
import br.com.noa.models.telegram.Message;

public interface InterfaceBotRoutine {
	
	String route(Message message) throws SuperException;

}
