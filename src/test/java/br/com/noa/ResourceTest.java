package br.com.noa;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.com.noa.models.telegram.Chat;
import br.com.noa.models.telegram.From;
import br.com.noa.models.telegram.Message;
import br.com.noa.models.telegram.ResponseSend;
import br.com.noa.models.telegram.ResponseUpdate;
import br.com.noa.models.telegram.Result;
import br.com.noa.rest.InterfaceConsumidorTelegram;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
public class ResourceTest {

	@InjectMock
	@RestClient
	InterfaceConsumidorTelegram consumidor;

	@BeforeEach
	public void setup() throws Throwable {
		List<Result> results = new ArrayList<>();
		Result result = new Result();
		result.setId(1L);

		Chat chat = new Chat();
		chat.setFirstName("Junior");
		chat.setId(1L);
		chat.setLastName("Filho");
		chat.setType("test");
		chat.setUsername("mockjunior");

		From from = new From();
		from.setFirstName("mock");
		from.setId(1L);
		from.setIsBot(false);
		from.setLanguageCode("pt-BR");
		from.setLastName("junior");
		from.setUsername("silvajunior");

		Message message = new Message();
		message.setDate(1L);
		message.setIsAnswer(true);
		message.setText("teste de mock");
		message.setChat(chat);
		message.setFrom(from);

		result.setMessage(message);
		results.add(result);

		ResponseUpdate update = new ResponseUpdate();
		update.setResult(results);

		Response response = Response.status(Status.OK).entity(update).build();

		Mockito.when(consumidor.getUpdates(Mockito.anyString(), Mockito.anyInt())).thenReturn(response);

		ResponseSend responseSend = new ResponseSend();

		responseSend.setOk(true);
		responseSend.setResult(message);

		Mockito.when(consumidor.sendMessage(Mockito.anyString(), Mockito.anyLong(), Mockito.any()))
				.thenReturn(response);
	}

	@Test
	public void getUpdates() throws Throwable {

		given().when().get("/telegram/getUpdates").then().statusCode(200);
	}

	@Test
	public void getUpdatesStatusError() throws Throwable {

		Mockito.when(consumidor.getUpdates(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);

		given().when().get("/telegram/getUpdates").then().statusCode(400);
	}
}